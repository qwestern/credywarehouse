-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Loomise aeg: Veebr 15, 2016 kell 10:02 EL
-- Serveri versioon: 5.6.17
-- PHP versioon: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `warehouse`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Andmete tõmmistamine tabelile `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'söök'),
(2, 'jook');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Andmete tõmmistamine tabelile `employee`
--

INSERT INTO `employee` (`id`, `name`, `is_active`) VALUES
(1, 'Peeter Paan', 1),
(2, 'Drew', 1),
(7, 'Andres', 0),
(8, 'Peeter1', 0),
(9, 'Martin', 1);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `description` varchar(500) NOT NULL,
  `picture_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Andmete tõmmistamine tabelile `product`
--

INSERT INTO `product` (`id`, `employee_id`, `name`, `price`, `description`, `picture_path`) VALUES
(17, 2, 'Muna', '545', 'drrdf', '170_1.jpg'),
(18, 1, 'Pepsi', '667', 'ytjty', '198_1.jpg'),
(20, 6, 'Muna2', '444', 'erfer', '187_1.jpg'),
(21, 6, 'Vorst2', '32', 'sfr esfwef', '187_1.jpg'),
(24, 2, 'Vorst', '44', 'asfsd sdfs', '187_1.jpg');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `producttocategory`
--

CREATE TABLE IF NOT EXISTS `producttocategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Andmete tõmmistamine tabelile `producttocategory`
--

INSERT INTO `producttocategory` (`id`, `product_id`, `category_id`) VALUES
(15, 20, 1),
(16, 21, 1),
(32, 24, 1),
(33, 24, 2),
(34, 18, 1),
(35, 18, 2),
(36, 17, 1),
(37, 17, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
