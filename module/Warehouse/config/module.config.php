<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Warehouse\Controller\Warehouse' => 'Warehouse\Controller\WarehouseController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'warehouse' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/warehouse[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Warehouse\Controller\Warehouse',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'products' => __DIR__ . '/../view',
        ),
    ),
);