<?php
namespace Warehouse;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Warehouse\Model\Warehouse;
use Warehouse\Form\WarehouseForm;
use Warehouse\Model\WarehouseTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Warehouse\Model\WarehouseTable' =>  function($sm) {
                    $tableGateway = $sm->get('WarehouseTableGateway');
                    $table = new WarehouseTable($tableGateway);
                    return $table;
                },
                'WarehouseTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Warehouse());
                    return new TableGateway('product', $dbAdapter, null, $resultSetPrototype);
                },
                'Warehouse\Form\WarehouseForm' => function($sm) {
                    $dbAdapter  = $sm->get('Zend\Db\Adapter\Adapter');
                    $form = new WarehouseForm($dbAdapter);

                    return $form;
    }
            ),

        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}