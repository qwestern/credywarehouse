<?php
namespace Warehouse\Model;

use Zend\Db\TableGateway\TableGateway;

class WarehouseTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getProducts()
    {

        $query = "SELECT product.id as id, product.name as product_name, description,price,picture_path, employee.name as employee_name
 FROM product, employee where employee_id=employee.id";

        $results = $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        return iterator_to_array($results);
    }



    public function getCategories()
    {

        $query = "SELECT product.id as prod_id, categories.name
        FROM product, categories,producttocategory
        where product.id=producttocategory.product_id and categories.id=producttocategory.category_id";

        $results = $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        $result = iterator_to_array($results);

        $sortedCategories = array();

        foreach ($result as $item) {
            $sortedCategories[$item['prod_id']][] = $item['name'];
        }
        return $sortedCategories;
    }

    public function updateCategories($id, $categories) {
        $query = "DELETE from producttocategory where product_id=$id";

        $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);

        foreach($categories as $category_id){
            $query = "INSERT into producttocategory (category_id,product_id) VALUES ($category_id, $id)";

            $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        }


    }

    public function getAllCategories()
    {

        $query = "SELECT * FROM  categories";

        $results = $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        $result = iterator_to_array($results);

        return $result;
    }

    public function getProduct($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getWorker($id) {
        $id  = (int) $id;
        $query = "SELECT * from employee where id=$id";

        $results = $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        $result = iterator_to_array($results);
        //print_r($result);
        if (!$result) {
            throw new \Exception("Could not find row $id");
        }
        return $result[0];
    }

    public function getWorkers() {
        $query = "SELECT * FROM employee where is_active=1";

        $employeeResult = $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
        $employees = iterator_to_array($employeeResult);

        $sortedEmployee = array();
        foreach ($employees as $employee) {
            $sortedEmployee[$employee['id']] = $employee['name'];
        }

        return $sortedEmployee;
    }



    public function saveProduct(Warehouse $product, $categoryIds)
    {
        $data = array(
            'employee_id' => $product->employee_id,
            'name' => $product->name,
            'price' => $product->price,
            'description' => $product->description,
            'picture_path' => $product->picture_path,
        );

        $id = (int) $product->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $last_id = $this->tableGateway->lastInsertValue;
            if(count($categoryIds)>0){
                foreach($categoryIds as $categoryId){
                    $query = "INSERT INTO producttocategory (product_id, category_id) values ($last_id, $categoryId)";
                    $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
                }
            }

        } else {
            if ($this->getProduct($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Product id does not exist');
            }
        }
    }

    public function saveEmployee($employee) {
        $query = "INSERT INTO employee (name) values ('$employee[name]')";

        $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
    }

    public function deleteProduct($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
        $query = "DELETE FROM producttocategory where product_id = $id";
        $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);
    }

    public function deleteWorker($id) {
        $query = "UPDATE employee set is_active=0 where id=$id";

        $this->tableGateway->getAdapter()->driver->getConnection()->execute($query);

    }
}