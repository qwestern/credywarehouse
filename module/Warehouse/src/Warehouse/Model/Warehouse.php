<?php
namespace Warehouse\Model;

use Warehouse\Model\WarehouseTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Warehouse  implements InputFilterAwareInterface
{
    public $id;
    public $employee_id;
    public $name;
    public $price;
    public $description;
    public $picture_path;

    protected $inputFilter;                       // <-- Add this variable


    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->employee_id = (!empty($data['employee_id'])) ? $data['employee_id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->price = (!empty($data['price'])) ? $data['price'] : null;
        $this->description = (!empty($data['description'])) ? $data['description'] : null;
        $this->picture_path = (!empty($data['picture_path'])) ? $data['picture_path'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();


            $inputFilter->add(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'category_id',
                'required' => false,

            ));



            $inputFilter->add(array(
                'name'     => 'price',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),



            ));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}