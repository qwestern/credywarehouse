<?php
namespace Warehouse\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Db\TableGateway\TableGateway;

class WarehouseForm extends Form
{


    public function __construct(\Zend\Db\Adapter\Adapter $dbAdapter)
    {
        // we want to ignore the name passed
        parent::__construct('product');

        $query = "SELECT * FROM employee where is_active=1";

        $employeeResult = $dbAdapter->driver->getConnection()->execute($query);
        $employees = iterator_to_array($employeeResult);

        $selectArrayEmployee = array();
        foreach ($employees as $employee) {
            $selectArrayEmployee[$employee['id']] = $employee['name'];
        }



        $query = "SELECT * FROM categories";

        $selectArrayCategory = array();

        $categoryResult = $dbAdapter->driver->getConnection()->execute($query);
        $categories = iterator_to_array($categoryResult);
        foreach ($categories as $category) {
            $selectArrayCategory[$category['id']] = $category['name'];
        }

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'picture_path',
            'type' => 'Hidden',
        ));

        $select = new Element\Select('employee_id');
        $select->setLabel('Choose employee');
        $select->setValueOptions($selectArrayEmployee);
        $this->add($select);
        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $select = new Element\Select('category_id');
        $select->setLabel('Choose categories')
            ->setAttribute('id', 'multi')
            ->setAttribute('multiple', 'multiple');
        $select->setValueOptions($selectArrayCategory);
        $this->add($select);

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'price',
            'type' => 'Text',
            'options' => array(
                'label' => 'Price',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'Text',
            'options' => array(
                'label' => 'Description',
            ),
        ));

        $file = new Element\File('file');
        $file->setLabel('Image Upload')
            ->setAttribute('id', 'image-file');
        $this->add($file);
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}