<?php
namespace Warehouse\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Db\TableGateway\TableGateway;


class WorkerForm extends Form
{


    public function __construct()
    {
        // we want to ignore the name passed
        parent::__construct('worker');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}