<?php
namespace Warehouse\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Warehouse\Model\Warehouse;          // <-- Add this import
use Warehouse\Model\Worker;          // <-- Add this import
use Warehouse\Form\WarehouseForm;       // <-- Add this import
use Warehouse\Form\WorkerForm;       // <-- Add this import

class WarehouseController extends AbstractActionController
{

    protected $warehouseTable;

    public function getWarehouseTable()
    {
        if (!$this->warehouseTable) {
            $sm = $this->getServiceLocator();
            $this->warehouseTable = $sm->get('Warehouse\Model\WarehouseTable');
        }
        return $this->warehouseTable;
    }

    public function indexAction()
    {
        return new ViewModel(array(
            //'products' => $this->getWarehouseTable()->fetchAll(),
            'products' => $this->getWarehouseTable()->getProducts(),
            'sortedCategories' => $this->getWarehouseTable()->getCategories(),
            'allCategories' => $this->getWarehouseTable()->getAllCategories(),
        ));
    }

    public function ajaxAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){ // If it's ajax call
            $id = $request->getPost('productID');
            $categories = $request->getPost('selectedCategories');
            //print_r($categories);
            $this->getWarehouseTable()->updateCategories($id, $categories);
    }

    }


    public function workerViewAction()
    {


            return new ViewModel(array(
                //'products' => $this->getWarehouseTable()->fetchAll(),
                'workers' => $this->getWarehouseTable()->getWorkers(),
            ));


    }

    public function addWorkerAction()
    {
        $form = new WorkerForm();
        //new WarehouseForm();
        $form->get('submit')->setValue('Add worker');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $employee = new Worker();
            $form->setInputFilter($employee->getInputFilter());

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $arrayToExchange=$form->getData();
                //print_r($arrayToExchange);


                $this->getWarehouseTable()->saveEmployee($arrayToExchange);

                // Redirect to list of albums

                return $this->redirect()->toRoute('warehouse');
            }
        }
        return array('form' => $form);
    }

    public function addAction()
    {
        $form = $this->getServiceLocator()->get('Warehouse\Form\WarehouseForm');
        //new WarehouseForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $product = new Warehouse();
            $form->setInputFilter($product->getInputFilter());


            $files =  $request->getFiles()->toArray();
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $filesize  = new \Zend\Validator\File\Size(array('min' => 1 )); //1KB
            $extension = new \Zend\Validator\File\Extension(array('extension' => array('jpeg','jpg','png','svg')));
            $httpadapter->setValidators(array($filesize, $extension), $files['file']['name']);

           $form->setData($request->getPost());

            if ($form->isValid()) {

                $arrayToExchange=$form->getData();
                //print_r($arrayToExchange);
                $arrayToExchange['picture_path']=$files['file']['name'];



                if($httpadapter->isValid()) {
                    //getcwd() . '/public
                    //__DIR__.'/../../../uploads
                    $httpadapter->setDestination(getcwd() . '/public/img');
                    $httpadapter->receive($files['file']['name']);
                }

                $product->exchangeArray($arrayToExchange);

                $this->getWarehouseTable()->saveProduct($product,$arrayToExchange['category_id']);

                // Redirect to list of albums
                return $this->redirect()->toRoute('warehouse');
            }
        }
        return array('form' => $form);
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            echo $id;

            return $this->redirect()->toRoute('warehouse', array(
                'action' => 'add'
            ));

        }

        // Get the Album with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $product = $this->getWarehouseTable()->getProduct($id);
        }
        catch (\Exception $ex) {
            echo 'pole leitud';
            echo $id;

            return $this->redirect()->toRoute('warehouse', array(
                'action' => 'index'
            ));

        }

        $form  = $this->getServiceLocator()->get('Warehouse\Form\WarehouseForm');
        $form->bind($product);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $product = new Warehouse();
            $form->setInputFilter($product->getInputFilter());


            $files = $request->getFiles()->toArray();
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $filesize = new \Zend\Validator\File\Size(array('min' => 1)); //1KB
            $extension = new \Zend\Validator\File\Extension(array('extension' => array('jpeg', 'jpg', 'png', 'svg')));
            $httpadapter->setValidators(array($filesize, $extension), $files['file']['name']);

            $form->setData($request->getPost());

            if ($form->isValid()) {


                $productObject = $form->getData();


                $arrayToExchange['picture_path'] = $productObject->picture_path;
                $arrayToExchange['id'] = $productObject->id;
                $arrayToExchange['employee_id'] = $productObject->employee_id;
                $arrayToExchange['name'] = $productObject->name;
                $arrayToExchange['price'] = $productObject->price;
                $arrayToExchange['description'] = $productObject->description;
                //$arrayToExchange['category_id'] = $productObject->data->category_id;

                if($files['file']['name']){
                   $arrayToExchange['picture_path'] = $files['file']['name'];
                }
                //print_r($arrayToExchange);

                if($files['file']['name']) {
                    if ($httpadapter->isValid()) {
                        //getcwd() . '/public
                        //__DIR__.'/../../../uploads
                        $httpadapter->setDestination(getcwd() . '/public/img');
                        $httpadapter->receive($files['file']['name']);
                    }
                }
                $product->exchangeArray($arrayToExchange);
                //print_r($product);

                $this->getWarehouseTable()->saveProduct($product, null);

                // Redirect to list of albums
                return $this->redirect()->toRoute('warehouse');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteWorkerAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('warehouse');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getWarehouseTable()->deleteWorker($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('warehouse');
        }

        return array(
            'id'    => $id,
            'worker' => $this->getWarehouseTable()->getWorker($id)
        );
    }



    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('warehouse');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getWarehouseTable()->deleteProduct($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('warehouse');
        }

        return array(
            'id'    => $id,
            'product' => $this->getWarehouseTable()->getProduct($id)
        );
    }
}